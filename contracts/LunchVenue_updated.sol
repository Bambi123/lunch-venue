// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/**
The following contract, LunchVenueUpdated is an updated version of the original LunchVenue contract, 
with a solution to 4 out of the 5 weakness present in the previous version.

These are the solved issues:

Issue 1: Duplicate votes from the same person were previously allowed.
    There are three conditions that require a vote to be valid.
         - The friend sending the vote needs to exist in the friends list. (ALREADY IMPLEMENTED)
         - The venue the friend is voting on needs to exist in the venu list. (ALREADY IMPLEMENTED)
         - The friends needs to only be allowed to vote if they have not voted yet. (NOT ALREADY IMPLEMENTED)
    To solve this issue, we will implement the checking whether a voting friend has already voted.
    To do this, we add a third condition in the doVote function so ensure that a friend's voted field
    has not already been marked as true.
    BUT, we can only run this condition once we ensure the existence of the friend, hence after condition 1.

Issue 2:  A well-defined create, voting open, and voting closed phases to recognise different voting stages.
    Currently, there are numerous functions that can be run after friends have already made votes, such as 
    addFriend, and addVenue.  This can cause many complications.

    To solve this issue, we will implement an enum state type to identify different phases of the voting process.
     - CREATE is the start of the voting process, which is set upon creation of the contract.  It allows the
       addFriend and addVenue calls to be made.  These functions fail if the voting process is in a different phase.
     - OPEN is the phase of the voting process that allows votes to be transacted.  Other types of functions will 
       throw errors if the voting process is not in the open phase.  The manager has to intentionally make a function call
       to start the voting phase.
     - CLOSED is the end of the voting phase.  After a quorem has been met, the closed phase prevents any functions from being called.
       The phase becomes closed automatically when the final results function is called.

Issue 3: Cancelling the Contract by Lunchtime if quorem is not met!
    There is currently no timeout method for the LunchVenue Contract.  It is desirable to add a timeout aspect in the
    case that no quorum has been met by the agreed upon lunch time.

    Solidity provides a very handly 'block' api that allows us to retrieve the current block number from the blockchain.
    This provides us a very handy time calculator to estimate the time passed at run-time of the contract by evaluating
    the average block creation time.  The average time to calculate a block on the Ethereum blockchain is approximately
    13 seconds, so we will use this stat to estimate when the contract should complete.  

    The modifier blockTimeoutCheck, instantly disables the contract if the ending block number is exceeded by the current clock
    number, allowing us to completely disable the contract after it has exceeded some duration of time.

Issue 4: A method of disabling a contract once it is deployed.
    Currently, there is no way to disable the contract after it is deployed.

    To solve this issue, we implement a partial solution to Issue 2, in which we define the closing state of the contract.
    One of the states addressing Issue 2 is the closing state phase of the contract.  In this phase, no other functions are allowed
    to be called.  This is ensured by the function modifiers indicating that the contract must be in a specific phase to
    be called.  No function can be run when the phase is closed, except for the results function.  This function is private and only run
    from the doVote function.  Since the state is closed just before the results function is run, this ensures that doVote cannot be called again
    after the state changes to closed and the results function is only run once, which is upon the very instant the voting is set to closed.
    This provides a state safe operation that allows us to fully disable the contract.  There is no way to change the state from closed to
    any other state.
*/

/// @title Contract to agree on the Lunch Venue
contract LunchVenueUpdated {

    struct Friend {
        string name;
        bool voted;
    }

    struct Vote {
        address voterAddress;
        uint venue;
    }

    mapping (uint => string) public venues; // list of venues (venue no. name)
    mapping (address => Friend) public friends; // list of friends (address, Friend)
    uint public numVenues = 0;
    uint public numFriends = 0;
    uint public numVotes = 0;
    address public manager; // manager of lunch venues contract
    string public votedVenue = ""; // where we have decided to go to lunch

    mapping (uint => Vote) private votes; // list of votes (vote no, Vote)
    mapping (uint => uint) private results; // list of vote counts (venue no, no of votes)

    /// Solution to Issue 2 and Issue 4
    enum PHASE {CREATE, OPEN, CLOSED}
    PHASE votingPhase = PHASE.CREATE;

    /// Solution to Issue 3
    uint public endBlockNumber;

    // Creates a new LunchVenue Contract
    constructor () votingPhaseCreate {
        manager = msg.sender; // set contract creator as manager of the contract
        endBlockNumber = calculateEndBlock(60 * 30);
    }

    /// @notice Calculates the Timeout Duration Based on transacted blocks
    /// @param secs The number of seconds we want to wait until the lunch venue contract should close automatically.
    function calculateEndBlock(uint secs) public view returns (uint) {
        uint blockCreationTime = 13; // seconds
        uint noOfBlocksDifference = uint(secs)/blockCreationTime;
        return noOfBlocksDifference + block.number;
    }

    /// @notice Set voting to open phase, can only be called by the manager,
    /// since this is a publicly called function, only allow the manager to call
    /// @return boolean indicating successful method
    function openVoting() public blockTimeoutCheck restricted votingPhaseCreate returns (bool){
        votingPhase = PHASE.OPEN;
        return true;
    }

    /// @notice Set voting to called phase, 
    /// there are two instances in which closeVoting is run, when the quorem is met, and when the contract is disabled.
    /// Since this is a private function, it can only be called from within contract methods, so it is protected
    /// by either the quorem being met in the doVote method and the disable call, which can only be invoked
    /// by the manager.
    /// @return boolean indicating successful method
    function closeVoting() private returns (bool){
        votingPhase = PHASE.CLOSED;
        return true;
    }

    /// @notice Add a new lunch venue
    /// @dev To simplify the code duplication of venues is not checked
    /// @param name Name of the venue
    /// @return number of lunch venues added so far
    function addVenue(string memory name) public blockTimeoutCheck restricted votingPhaseCreate  returns (uint){
        numVenues++;
        venues[numVenues] = name;
        return numVenues;
    }

    /// @notice Add a new friend who can vote on a lunch venue
    /// @dev To Simplify the code, duplication of friends is not checked
    /// @param friendAddress Friend'a account address
    /// @param name of friend's name
    /// @return Number of friends added so far
    function addFriend(address friendAddress, string memory name) blockTimeoutCheck public restricted votingPhaseCreate returns (uint){
        Friend memory f;
        f.name = name;
        f.voted = false;
        friends[friendAddress] = f;
        numFriends++;
        return numFriends;
    }

    /// @notice Vote for a lunch venue
    /// @dev To Simplify the code, multiple votes by a friend is not checked
    /// @param venue Venue number being voted
    /// @return validVote Is the vote valid? A valid vote should be from a registered friend and to a registered venue
    function doVote(uint venue) public blockTimeoutCheck votingPhaseOpen returns (bool validVote){
        // Initially Set Valid vote to false
        validVote = false;
        // If the sender exists in the friends list
        if (bytes(friends[msg.sender].name).length != 0){
            // If the venue input is valid and the sender has not voted yet
            if (bytes(venues[venue]).length != 0 && !friends[msg.sender].voted){
                // Set valid vote to true
                validVote = true;
                // Set voted field on the sender friend to true
                friends[msg.sender].voted = true;
                // Initialise a new vote
                Vote memory v;
                // Set the voter address to the sender
                v.voterAddress = msg.sender;
                // Set the voted venue to the input venue
                v.venue = venue;
                // Increase the total number of votes in the contract
                numVotes++;
                // Assign the vote into the list of votes
                votes[numVotes] = v;
            }
        }

        // If the quorem is met
        if (numVotes >= numFriends/2 + 1) {
            // Close the voting phase
            closeVoting();
            // Prepare for the final results
            finalResults();
        }

        // Return the valid vote
        return validVote;
    }

    /// @notice Determine winner venue
    /// @dev If top 2 venues have the same no. of votes, final result depends on vote order
    function finalResults() private blockTimeoutCheck votingPhaseClosed {
        // Initialise the highest no. of votes
        uint highestVotes = 0;
        // Initialise the highest voted venue
        uint highestVenue = 0;

        for (uint i = 1; i <= numVotes; i++){ // for each vote
            uint voteCount = 1;
            if (results[votes[i].venue] > 0){ // if there are already votes for this venue
                voteCount += results[votes[i].venue];
            }
            results[votes[i].venue] = voteCount; // reinitialise the results with the total no. of votes for this venue

            if (voteCount > highestVotes){
                highestVotes = voteCount;
                highestVenue = votes[i].venue;
            }
        }

        votedVenue = venues[highestVenue]; // we have chosen the lunch venue
    }

    /// @notice The disabled function invokes the selfdestruct method which cancels the contract
    /// on the blockchain.
    /// Solution to Issue 4
    function disable() public restricted returns (bool){
        closeVoting();
        return true;
    }

    /// @notice Only manager can do
    modifier restricted() {
        require(msg.sender == manager, "Can only be executed by the manager");
        _;
    }

    /// @notice ensures voting is in the create phase
    modifier votingPhaseCreate() {
        require(votingPhase == PHASE.CREATE, "Can only add friends and venues before voting is open");
        _;
    }

    /// @notice Only when voting is in the open phase
    modifier votingPhaseOpen() {
        require(votingPhase == PHASE.OPEN, "Can vote only while voting is open.");
        _;
    }

    /// @notice Only when voting is in the closed phase
    modifier votingPhaseClosed() {
        require(votingPhase == PHASE.CLOSED, "Voting has now closed.");
        _;
    }

    /// @notice Only when we have not exceeded the block timeout
    /// Solution to Issue 3
    modifier blockTimeoutCheck() {
        if (endBlockNumber < block.number){
            disable();
        }
        _;
    }

}