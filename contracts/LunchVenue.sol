// SPDX-License-Identifier: UNLICENSED

pragma solidity ^0.8.0;

/// @title Contract to agree on the Lunch Venue
contract LunchVenue {

    struct Friend {
        string name;
        bool voted;
    }

    struct Vote {
        address voterAddress;
        uint venue;
    }

    mapping (uint => string) public venues; // list of venues (venue no. name)
    mapping (address => Friend) public friends; // list of friends (address, Friend)
    uint public numVenues = 0;
    uint public numFriends = 0;
    uint public numVotes = 0;
    address public manager; // manager of lunch venues contract
    string public votedVenue = ""; // where we have decided to go to lunch

    mapping (uint => Vote) private votes; // list of votes (vote no, Vote)
    mapping (uint => uint) private results; // list of vote counts (venue no, no of votes)
    bool voteOpen = true; // initially set to true

    // Creates a new LunchVenue Contract
    constructor () {
        manager = msg.sender; // set contract creator as manager of the contract
    }

    /// @notice Add a new lunch venue
    /// @dev To simplify the code duplication of venues is not checked
    /// @param name Name of the venue
    /// @return number of lunch venues added so far
    function addVenue(string memory name) public restricted returns (uint){
        numVenues++;
        venues[numVenues] = name;
        return numVenues;
    }

    /// @notice Add a new friend who can vote on a lunch venue
    /// @dev To Simplify the code, duplication of friends is not checked
    /// @param friendAddress Friend'a account address
    /// @param name of friend's name
    /// @return Number of friends added so far
    function addFriend(address friendAddress, string memory name) public restricted returns (uint){
        Friend memory f;
        f.name = name;
        f.voted = false;
        friends[friendAddress] = f;
        numFriends++;
        return numFriends;
    }

    /// @notice Vote for a lunch venue
    /// @dev To Simplify the code, multiple votes by a friend is not checked
    /// @param venue Venue number being voted
    /// @return validVote Is the vote valid? A valid vote should be from a registered friend and to a registered venue
    function doVote(uint venue) public votingOpen returns (bool validVote){
        validVote = false;
        if (bytes(friends[msg.sender].name).length != 0){
            if (bytes(venues[venue]).length != 0){
                validVote = true;
                friends[msg.sender].voted = true;
                Vote memory v;
                v.voterAddress = msg.sender;
                v.venue = venue;
                numVotes++;
                votes[numVotes] = v;
            }
        }

        if (numVotes >= numFriends/2 + 1) { // quorum is met
            finalResults();
        }

        return validVote;
    }

    /// @notice Determine winner venue
    /// @dev If top 2 venues have the same no. of votes, final result depends on vote order
    function finalResults() private {
        uint highestVotes = 0;
        uint highestVenue = 0;

        for (uint i = 1; i <= numVotes; i++){ // for each vote
            uint voteCount = 1;
            if (results[votes[i].venue] > 0){ // if there are already votes for this venue
                voteCount += results[votes[i].venue];
            }
            results[votes[i].venue] = voteCount; // reinitialise the results with the total no. of votes for this venue

            if (voteCount > highestVotes){
                highestVotes = voteCount;
                highestVenue = votes[i].venue;
            }
        }

        votedVenue = venues[highestVenue]; // we have chosen the lunch venue
        voteOpen = false; // voting is no longer open

    }


    /// @notice Only manager can do
    modifier restricted() {
        require(msg.sender == manager, "Can only be executed by the manager");
        _;
    }

    /// @notice Only when voting is still open
    modifier votingOpen() {
        require(voteOpen == true, "Can vote only while voting is open.");
        _;
    }
}