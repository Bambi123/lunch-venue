// SPDX-License-Identifier: UNLICENSED

pragma solidity >= 0.8.00 < 0.9.0;
import "remix_tests.sol"; // this import is automatically injected by remix
import "remix_accounts.sol"; // same for this one
import "../contracts/LunchVenue.sol"; // import the developed smart contract

// file has to end with "_tests.sol", it can contain more than one testSuite
// for the smart contract being tested!

// Inherit lunch venue contract
contract LunchVenueTest is LunchVenue {

    // variables used to emulate addresses of different accounts
    address acc0;
    address acc1;
    address acc2;
    address acc3;
    address acc4;

    // beforeAll runs before all other tests, more special functions:
    // beforeEach, beforeAll, afterEach, afterAll,
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
        acc2 = TestsAccounts.getAccount(2);
        acc3 = TestsAccounts.getAccount(3);
        acc4 = TestsAccounts.getAccount(4);
    }

    /// account at zero index is default account, so manager account will be
    /// set to account zero
    function managerTest() public {
        Assert.equal(manager, acc0, "Manager should be acc0");
    }

    /// Add lunch venue as managher
    /// when msg.sender isn't specified, default account is 
    /// considered as the sender
    function setLunchVenue() public {
        Assert.equal(addVenue("Courtyard Cafe"), 1, "Should be equal to 1");
        Assert.equal(addVenue("Uni Cafe"), 2, "Should be equal to 2");
    }

    /// Try to add lunch venue as a user other than manager, This
    /// Should fail.  
    /// #sender: account-1
    function setLunchVenueFailure() public {
        try this.addVenue("Atomic Cafe") returns (uint v) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can only be executed by the manager", "Failed with unexpected reason");
        } catch (bytes memory /* lowLevelData*/){
            Assert.ok(false, "Failed unexpected");
        }
    }

    // Set friends as account-0
    /// #sender doesn't need to be specified explicity for account 0
    function setFriend() public {
        Assert.equal(addFriend(acc0, "Alice"), 1, "Should be equal to 1");
        Assert.equal(addFriend(acc1, "Bob"), 2, "Should be equal to 2");
        Assert.equal(addFriend(acc2, "Charlie"), 3, "Should be equal to 3");
        Assert.equal(addFriend(acc3, "Eve"), 4, "Should be equal to 4");
    }

    /// Try adding friend as a user other than manager.  This should fail
    /// #sender: account-2

    function setFriendFailure() public {
        try this.addFriend(acc4, "Daniels") returns (uint f) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can only be executed by the manager", "Failed with unexpected reason");
        } catch (bytes memory /*lowLevelData*/){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /// Vote as Bob (acc1)
    /// #sender: account-1
    function vote() public {
        Assert.ok(doVote(2), "Voting results should be true");
    }

    /// Vote as Charlie
    /// #sender: account-3
    function vote2() public {
        Assert.ok(doVote(1), "Voting results should be true");
    }

    // Try voting as a user not in the friends list.  This should fail.
    /// #sender: account-4
    function voteFailure() public {
        Assert.equal(doVote(1), false, "Voting results should be false");
    }

    // Vote as eve
    /// #sender: account-3
    function vote3() public {
        Assert.ok(doVote(2), "Voting results should be true");
    }

    /// Verify lunch venue is set correctly
    function voteOpenTest() public {
        Assert.equal(votedVenue, "Uni Cafe", "Selected venue should be Uni Cafe");
    }

    /// Verify voting after vote closed.  This should fail
    /// #sender: account-2
    function voteAfterClosedFailure() public {
        try this.doVote(1) returns (bool validVote){
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can vote only while voting is open.", "Failed with unexpected reason");
        } catch (bytes memory /*lowLevelData*/){
            Assert.ok(false, "Failed unexpectedly");
        }
    }

}



