// SPDX-License-Identifier: UNLICENSED

/* Solodity Version */
pragma solidity >= 0.8.00 < 0.9.0;
/* Imported Testing Libraries */
import "remix_tests.sol";
import "remix_accounts.sol";
/* Imported Smart Contract */
import "../contracts/LunchVenue_updated.sol";


/* Inherits the Updated Lunch Venue Contract */
contract LunchVenueUpdatedTest is LunchVenueUpdated {

    /* Account variables set up to emulate the addresses of the different accounts */
    address acc0; address acc1; address acc2; address acc3; address acc4;

    /* beforeAll runs before all other tests, more special functions: beforeEach, beforeAll, afterEach, afterAll*/
    function beforeAll() public {
        acc0 = TestsAccounts.getAccount(0);
        acc1 = TestsAccounts.getAccount(1);
        acc2 = TestsAccounts.getAccount(2);
        acc3 = TestsAccounts.getAccount(3);
        acc4 = TestsAccounts.getAccount(4);
    }

    /* Account at zero index is default account to send, so manager account will be at account-0 */
    function managerTest() public {
        Assert.equal(manager, acc0, "Manager should be acc0");
    }

    /* Set up all lunch venues */
    /// #sender: account-0
    function setLunchVenueTest() public {
        Assert.equal(addVenue("Cafe"), 1, "Should be equal to 1");
        Assert.equal(addVenue("Restaurant"), 2, "Should be equal to 2");
        Assert.equal(addVenue("Bakery"), 3, "Should be equal to 3");
        Assert.equal(addVenue("Garden"), 4, "Should be equal to 4");
    }

    /* Set up all friends */
    /// #sender: account-0
    function setFriendTest() public {
        Assert.equal(addFriend(acc0, "Alice"), 1, "Should be equal to 1");
        Assert.equal(addFriend(acc1, "Bob"), 2, "Should be equal to 2");
        Assert.equal(addFriend(acc2, "Charlie"), 3, "Should be equal to 3");
        Assert.equal(addFriend(acc3, "Eve"), 4, "Should be equal to 4");
    }

    /* Try to add lunch venue as a user other than manager */
    /// #sender: account-1
    function setLunchVenueFailureTest() public {
        try this.addVenue("Atomic Cafe") returns (uint v) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can only be executed by the manager", "Failed with unexpected reason");
        } catch (bytes memory /* lowLevelData */){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /* Try adding friend as a user other than manager.  This should fail */
    /// #sender: account-2
    function setFriendFailureTest() public {
        try this.addFriend(acc4, "Daniels") returns (uint f) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can only be executed by the manager", "Failed with unexpected reason");
        } catch (bytes memory /*lowLevelData*/){
            Assert.ok(false, "Failed unexpected");
        }
    }

    /* Check that the End Block represents a correct value */
    /// #sender: account-0
    function endBlockTest() public {
        Assert.greaterThan(endBlockNumber, block.number, "endBlockNumber should represent a uint in the correct range");
        Assert.greaterThan(block.number + uint(1800)/13, endBlockNumber, "endBlockNumber should represent a uint in the correct range");
    }

    /* Check number of friends */
    /// #sender: account-0
    function checkNumFriendsTest() public {
        Assert.equal(numFriends, 4, "Number of Friends should be 4");
    }

    /* Try voting as a friend before the voting opens.  This should fail */
    /// #sender: account-0
    function voteTest0() public {
        try this.doVote(1) returns (bool validVote){
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can vote only while voting is open.", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpectly");
        }
    }

    /* Try to start the voting process from a friend's address, not the manager.  This should fail*/
    /// #sender: account-1
    function openVotingFailureTest() public {
        try this.openVoting() returns (bool status) {
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can only be executed by the manager", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpectedly");
        }
    }

    /* Open the voting phase of the contract */
    /// #sender: account-0
    function openVotingTest() public {Assert.ok(openVoting(), "Opening of Voting Phase should be true");}

    /* Try voting as a user not in the friends list.  This should fail */
    /// #sender: account-4
    function voteTest1() public {Assert.equal(doVote(1), false, "Voting results should be false");}

    /* Check the number of votes */
    /// #sender: account-0
    function checkNumVotesTest0() public {Assert.equal(numVotes, 0, "Number of Votes Should be 0");}

    /* Vote as Manager */
    /// #sender: account-0
    function voteTest2() public {Assert.ok(doVote(1), "Voting results should be true");}

    /* Check the number of votes */
    /// #sender: account-0
    function checkNumVotesTest1() public {Assert.equal(numVotes, 1, "Number of Votes Should be 1");}

    /* Vote as Bob */
    /// #sender: account-1
    function voteTest3() public {Assert.ok(doVote(2), "Voting results should be true");}

    /* Vote as Bob AGAIN.  This should fail.*/
    /// #sender: account-1
    function voteTest4() public {Assert.equal(doVote(1), false, "Voting Results should be false");}

    /* Check the number of votes */
    /// #sender: account-0
    function checkNumVotesTest2() public {Assert.equal(numVotes, 2, "Number of Votes Should be 2");}

    /* Vote as Charlie */
    /// #sender: account-2
    function voteTest5() public {        
        Assert.ok(doVote(2), "Voting Results should be true");
    }

    /* Vote as Eve, Quorem is already met, so Eve's vote should fail and throw error */
    /// #sender: account-3
    function voteTest6() public {
        try this.doVote(3) returns (bool validVote){
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can vote only while voting is open.", "Failed with unexpected reason");
        } catch (bytes memory){
            Assert.ok(false, "Failed unexpectly");
        }
    }

    /// Verify lunch venue is set correctly, sender irrelevant
    function voteOpenTest() public {
        Assert.equal(votedVenue, "Restaurant", "Selected venue should be Restaurant");
    }

    /// Verify voting after vote closed.  This should fail
    /// #sender: account-2
    function voteAfterClosedFailureTest() public {
        try this.doVote(1) returns (bool validVote){
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can vote only while voting is open.", "Failed with unexpected reason");
        } catch (bytes memory /*lowLevelData*/){
            Assert.ok(false, "Failed unexpectedly");
        }
    }

    /* Attempt to disable the contract from a sender who is not the manager.  This should fail. */
    /// #sender: account 1
    function disableFailureTest2() public {
        try this.disable() returns (bool status){
            Assert.ok(false, "Method execution should fail");
        } catch Error(string memory reason){
            Assert.equal(reason, "Can only be executed by the manager", "Failed with unexpected reason");
        } catch (bytes memory /*lowLevelData*/){
            Assert.ok(false, "Failed unexpectedly");
        }
    }

    /* Disable the contract with the manager as the sender */
    /// #sender: account-0
    function disableTest() public {
        Assert.ok(disable(), "Disabling should return true");
    }



}



